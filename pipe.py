import pygame
import random

PIPESIZE = 64

pipeim = pygame.image.load("pipe.png")
pipetop = pygame.image.load("pipetop.png")

class Pipe:
    def __init__(self, position):
        if position < 5000:
            self.height = random.randint(1, 6)
            self.gapsize = random.randint(2, 3)
        else:
            self.height = random.randint(0, 7)
            self.gapsize = random.randint(1, 3)
        self.image = pygame.Surface((PIPESIZE, 720))
        self.image.fill((255, 0, 255))
        for i in range(0, 12, 1):
            drawpos = (0, i * PIPESIZE)
            if i == self.height:
                self.image.blit(pygame.transform.flip(pipetop, False, True), drawpos)
            elif i == self.height + self.gapsize + 1:
                self.image.blit(pipetop, drawpos)
            elif self.height < i < self.height + self.gapsize + 1:
                continue
            else:
                self.image.blit(pipeim, drawpos)
        
        self.image.set_colorkey((255, 0, 255))
        
        self.position = position
        
    def draw(self, surface, scroll):
        surface.blit(self.image, (self.position - scroll, 0))
