import pygame
from pygame.locals import *

pygame.init()

import pipe
import bird

WIDTH = 1280
HEIGHT = 720 
FPS = 120

def doesrangeoverlap(range1, range2):
    range1 = list(range1)
    range2 = list(range2)
    
    return list(sorted(range1 + range2)) not in (range1 + range2, range2 + range1)

def doesrectcollide(rect1, rect2):
    return     (doesrangeoverlap((rect1[0], rect1[0] + rect1[2]), (rect2[0], rect2[0] + rect2[2]))
            and doesrangeoverlap((rect1[1], rect1[1] + rect1[3]), (rect2[1], rect2[1] + rect2[3])))

S = pygame.display.set_mode((WIDTH, HEIGHT))
pygame.display.set_caption("Flappy Bird")

clock = pygame.time.Clock()

background = pygame.image.load("background.png").convert()


thebird = bird.Bird()

FONTFT = pygame.font.Font(None, 250)
FONTFS = pygame.font.Font(None, 50)

scroll = 0
fakescroll = 0
pipes = []
dying = False
ext = False
playing = False
scored = None
while not ext:
    if playing:
        if not scroll % 200:
            pipes.append(pipe.Pipe(scroll + WIDTH + 64))
    
        for event in pygame.event.get():
            if event.type == QUIT:
                ext = True
                break
            elif event.type == KEYDOWN:
                if event.key == K_SPACE and not dying:
                    thebird.velocity = max(min(thebird.velocity, 0), -500)
                    thebird.velocity -= 500
    
        thebird.velocity += 1500 / FPS
        thebird.move(FPS)
        
        if thebird.position > HEIGHT:
            playing = False
            scored = scroll
            thebird = bird.Bird()
            pipes = []

        birdrect = (scroll + 100, thebird.position, thebird.image.get_width(), thebird.image.get_height())
        for p in pipes:
            if doesrectcollide(birdrect, (p.position + 12, -10000, 40, (p.height + 1) * 64 + 10000 - 11)) or doesrectcollide(birdrect, (p.position + 12, (p.height + p.gapsize + 1) * 64 + 11, 40, 10000)):
                dying = True

    S.blit(background, ((-(scroll + fakescroll) / 2 % WIDTH), 0))
    S.blit(background, ((-(scroll + fakescroll) / 2 % WIDTH) - WIDTH, 0))

    if not playing:
        for event in pygame.event.get():
            if event.type == QUIT:
                ext = True
                break
            elif event.type == KEYDOWN:
                playing = True
                lost = False
                dying = False
                fakescroll = scroll + 0
                scroll = 0
                
        title = FONTFT.render("Flappy Bird", 1, (0, 0, 0))
        S.blit(title, ((WIDTH - title.get_width()) / 2, 100))
        if scored is not None:
            score = FONTFS.render("You scored: " + str(scored), 1, (0, 0, 0))
            S.blit(score, ((WIDTH - score.get_width()) / 2, 500))
        

    if playing:
        for p in pipes:
            p.draw(S, scroll)
    
        pipes = list(filter(lambda p: p.position > scroll - 64, pipes))

    thebird.draw(S, scroll, dying)
    pygame.display.update()
    clock.tick(FPS)

    scroll += 1
