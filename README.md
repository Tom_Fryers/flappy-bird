# Flappy Bird Clone

Developed speedrun-style, in 98 m 59 s.

See video [here](https://www.youtube.com/watch?v=uEEZJyPYCP4).

To launch the game, run `python main.py`.
