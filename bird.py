import pygame


class Bird:
    def __init__(self):
        self.image = pygame.image.load("bird.png")
        self.deadimage = pygame.image.load("deadbird.png")
        self.position = 400
        self.velocity = 0
    
    def move(self, FPS):
        self.position += self.velocity / FPS
    
    def draw(self, surface, scroll, dying):
        if dying:
            im = self.deadimage
        else:
            im = self.image
        surface.blit(pygame.transform.rotate(im, -self.velocity / 50), (100, self.position))
    
